export CURRENT_DIR="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
if [ -d "$CURRENT_DIR/apache-ant-1.8.2/" ]; then
  export "ANT_HOME=$CURRENT_DIR/apache-ant-1.8.2"
  echo "$ANT_HOME"
fi

cd hsqldb
cd data
sh $ANT_HOME/bin/ant swing
